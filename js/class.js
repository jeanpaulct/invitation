/**
 * 3D-Invitation
 * -----------
 *
 * author: PeHaa Hetman
 * email: info@pehaa.com
 *
 * version 1.0 release date: 14.04.2013
 *
 **/
var invitation = {
    defaults: {
        gallery: [],
        titles: [],
        mapstyles: [],
        gallerySelector: ".icon-images, .fancybox",
        mapOnClick: !1
    },
    option: {},
    init: function (e) {
        var t = this;
        t.option = $.extend({}, t.defaults, e);
        t.addMap();
        t.option.gallery.length > 0 && t.option.gallerySelector !== "" && t.openGallery();
        $(".flip-me").click(function () {
            $("#invitation").toggleClass("front-face").toggleClass("back-face");
            $(".back-face #letter").css("top", -830 - ($(window).height() - 640 - 60) / 2 + "px");
            $(".front-face #letter").css("top", "-130px");
            $(this).attr("id") === "flip-me" && $(".page-content").jScrollPane();
            return !1
        });

        $(".open-close").click(function () {
            $("#content").toggleClass("opened");
            $("#invitation").addClass("hidden");
            return !1
        });
        $("#open-l").click(function () {
            $("#letter").addClass("opened");
            return !1
        });
				$("#open-l").hover(function(){
					$(".csstransforms3d .page-left").addClass("open-hover");
				}, function(){
					$(".csstransforms3d .page-left").removeClass("open-hover");
				});
        $("#hide-l").click(function () {
            $("#content").removeClass("opened");
            $("#invitation").removeClass("hidden");
            return !1
        });
        $("#close-l").click(function () {
            $("#letter").removeClass("opened");
            return !1
        });
				$("#close-l").hover(function(){
					$(".csstransforms3d .page-right").addClass("close-hover");
				}, function(){
					$(".csstransforms3d .page-right").removeClass("close-hover");
				});
    },
    addMap: function () {
        var e = this,
            t = function (t, n, r) {
                var i = new google.maps.LatLng(n, r),
                    s = {
                        zoom: 15,
                        center: i,
                        scrollwheel: !1,
                        mapTypeControl: !0,
                        mapTypeControlOptions: {
                            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                        },
                        navigationControl: !0,
                        navigationControlOptions: {
                            style: google.maps.NavigationControlStyle.SMALL
                        },
                        styles: [{
                                stylers: e.option.mapstyles
                            }
                        ],
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }, o = new google.maps.Map(document.getElementById(t), s),
                    u = new google.maps.LatLng(n, r),
                    a = new google.maps.Marker({
                        position: u,
                        map: o,
                        zIndex: 3
                    })
            };
        if (e.option.mapOnClick) $(".show-map").click(function () {
                var e = $(this).parents(".page-content"),
                    n = e.data("jsp");
                !n && $(window).height() - $(this).offset().top + $(window).scrollTop() < 380 && $("html, body").stop().animate({
                    scrollTop: 430 - $(window).height() + $(this).offset().top
                }, 500);
                var r = $(this).next(".map").fadeIn(1500),
                    i = r.attr("data-lat"),
                    s = r.attr("data-lng"),
                    o = r.attr("id");
                t(o, i, s);
                n && n.reinitialise();
                return !1
            });
        else {
            $(".map").show();
            $(".map").each(function () {
                var e = $(this),
                    n = e.attr("data-lat"),
                    r = e.attr("data-lng"),
                    i = e.attr("id");
                t(i, n, r)
            })
        }
    },
    openGallery: function () {
        var e = this;
        $(document).on("click", e.option.gallerySelector, function () {
            var t = [];
            $(e.option.gallery).each(function (n, r) {
                t[n] = {
                    href: r,
                    title: e.option.titles[n]
                }
            });
            $.fancybox(t, {
                nextClick: !0,
                arrows: !Modernizr.touch,
                scrolling: "no"
            });
            return !1
        })
    }
};
(function () {
    var e = 0,
        t = ["ms", "moz", "webkit", "o"];
    for (var n = 0; n < t.length && !window.requestAnimationFrame; ++n) {
        window.requestAnimationFrame = window[t[n] + "RequestAnimationFrame"];
        window.cancelAnimationFrame = window[t[n] + "CancelAnimationFrame"] || window[t[n] + "CancelRequestAnimationFrame"]
    }
    window.requestAnimationFrame || (window.requestAnimationFrame = function (t, n) {
        var r = (new Date).getTime(),
            i = Math.max(0, 16 - (r - e)),
            s = window.setTimeout(function () {
                t(r + i)
            }, i);
        e = r + i;
        return s
    });
    window.cancelAnimationFrame || (window.cancelAnimationFrame = function (e) {
        clearTimeout(e)
    })
})();